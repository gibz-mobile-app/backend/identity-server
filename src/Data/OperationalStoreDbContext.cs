using Duende.IdentityServer.EntityFramework.DbContexts;
using Duende.IdentityServer.EntityFramework.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace IdentityServer.Data;

public class OperationalStoreDbContext : PersistedGrantDbContext<OperationalStoreDbContext>
{
    public OperationalStoreDbContext(DbContextOptions<OperationalStoreDbContext> options) : base(options)
    {
    }
}

// The DbContextFactory is used at design-time (generate migrations, ...).
public class OperationalStoreDbContextFactory : IDesignTimeDbContextFactory<OperationalStoreDbContext>
{
    public OperationalStoreDbContext CreateDbContext(string[] args)
    {
        string connectionString = "Server=127.0.0.1;Port=13305;Database=identityserver;User=gibzapp;Password=password";
        var optionsBuilder = new DbContextOptionsBuilder<OperationalStoreDbContext>();
        ServerVersion sqlServerVersion = new MySqlServerVersion(new Version(8, 0, 26));
        optionsBuilder.UseMySql(connectionString, sqlServerVersion);

        var options = new OperationalStoreOptions();
        var dbContext = new OperationalStoreDbContext(optionsBuilder.Options);
        dbContext.StoreOptions = options;

        return dbContext;
    }
}