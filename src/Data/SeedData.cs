using Duende.IdentityServer.EntityFramework.Mappers;
using Duende.IdentityServer.Models;
using Microsoft.EntityFrameworkCore;

namespace IdentityServer.Data;

public class SeedData
{
    public static void EnsureSeedData(IServiceProvider serviceProvider)
    {
        // PerformMigrations(serviceProvider);
        EnsureSeedData(serviceProvider.GetRequiredService<ConfigurationStoreDbContext>());
    }

    private static void PerformMigrations(IServiceProvider serviceProvider)
    {
        serviceProvider.GetRequiredService<IdentityDbContext>().Database.Migrate();
        serviceProvider.GetRequiredService<ConfigurationStoreDbContext>().Database.Migrate();
        serviceProvider.GetRequiredService<OperationalStoreDbContext>().Database.Migrate();
    }

    private static void EnsureSeedData(ConfigurationStoreDbContext context)
    {
        if (!context.IdentityResources.Any())
        {
            context.IdentityResources.Add((new IdentityResources.OpenId()).ToEntity());
            context.IdentityResources.Add((new IdentityResources.Profile()).ToEntity());
            context.IdentityResources.Add((new IdentityResources.Email()).ToEntity());

            context.SaveChanges();
        }
    }
}
