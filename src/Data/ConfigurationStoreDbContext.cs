using Duende.IdentityServer.EntityFramework.DbContexts;
using Duende.IdentityServer.EntityFramework.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace IdentityServer.Data;

public class ConfigurationStoreDbContext : ConfigurationDbContext<ConfigurationStoreDbContext>
{
    public ConfigurationStoreDbContext(DbContextOptions<ConfigurationStoreDbContext> options) : base(options)
    {
    }
}

// The DbContextFactory is used at designtime (generate migrations, ...).
public class ConfigurationStoreDbContextFactory : IDesignTimeDbContextFactory<ConfigurationStoreDbContext>
{
    public ConfigurationStoreDbContext CreateDbContext(string[] args)
    {
        string connectionString = "Server=127.0.0.1;Port=13305;Database=identityserver;User=gibzapp;Password=password";
        var optionsBuilder = new DbContextOptionsBuilder<ConfigurationStoreDbContext>();
        ServerVersion sqlServerVersion = new MySqlServerVersion(new Version(8, 0, 26));
        optionsBuilder.UseMySql(connectionString, sqlServerVersion);

        var options = new ConfigurationStoreOptions();
        var dbContext = new ConfigurationStoreDbContext(optionsBuilder.Options);
        dbContext.StoreOptions = options;

        return dbContext;
    }
}
