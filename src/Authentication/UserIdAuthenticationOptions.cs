using Microsoft.AspNetCore.Authentication;

public class UserIdAuthenticationOptions : AuthenticationSchemeOptions
{
    public const string DefaultScheme = "UserIdScheme";

    public const string UserIdConfigurationOption = "Authorization:IdsrvAdminUserIds";

    public string TokenHeaderName { get; set; } = "X-IdsrvAdminUserId";

    public string AllowedUserIds { get; set; } = "";
}