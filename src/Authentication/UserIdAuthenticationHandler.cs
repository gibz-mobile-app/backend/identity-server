using System.Security.Claims;
using System.Text.Encodings.Web;
using IdentityServer.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;

/// <summary>
/// This custom authentication handler allows authentication using a simple http request header.
/// Is should be used simply upon system setup, when there's not yet any user with idsrvAdmin role.
/// Any user might register and aftwerwards use its user id withing the corresponding request header to authenticate.
/// </summary>
public class UserIdAuthenticationHandler : AuthenticationHandler<UserIdAuthenticationOptions>
{
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly IConfiguration _configuration;

    public UserIdAuthenticationHandler(
        IOptionsMonitor<UserIdAuthenticationOptions> options,
        ILoggerFactory logger,
        UrlEncoder encoder,
        ISystemClock clock,
        IConfiguration configuration,
        UserManager<ApplicationUser> userManager)
        : base(options, logger, encoder, clock)
    {
        _userManager = userManager;
    }

    protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
    {
        // Check for header
        if (!Request.Headers.ContainsKey(Options.TokenHeaderName))
        {
            return AuthenticateResult.Fail($"Missing header: {Options.TokenHeaderName}");
        }

        // Get header value
        string userId = Request.Headers[Options.TokenHeaderName]!;

        // Validate user id
        // var allowedUserIds = _configuration.GetValue<string>(UserIdAuthenticationOptions.UserIdConfigurationOption, "")!;
        if (!Options.AllowedUserIds.Contains(userId))
        {
            return AuthenticateResult.Fail($"Invalid user id");
        }

        // Successful authentication
        var claims = new List<Claim>()
        {
            new Claim(ClaimTypes.NameIdentifier, userId)
        };
        var claimsIdentity = new ClaimsIdentity(claims, this.Scheme.Name);
        var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

        return AuthenticateResult.Success(new AuthenticationTicket(claimsPrincipal, this.Scheme.Name));
    }
}