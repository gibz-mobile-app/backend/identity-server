using System.ComponentModel.DataAnnotations;

namespace IdentityServer.Models.DTOs.Request;

public class RoleCreationRequestDto
{
    [Required]
    public string Name { get; set; } = null!;
}