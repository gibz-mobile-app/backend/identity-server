using System.ComponentModel.DataAnnotations;

namespace IdentityServer.Models.DTOs.Request;

public class RoleModificationRequestDto
{
    [Required]
    public string UserId { get; set; } = null!;

    public string[] RolesToBeAdded { get; set; } = null!;

    public string[] RolesToBeRemoved { get; set; } = null!;
}