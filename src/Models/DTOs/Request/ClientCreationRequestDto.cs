using System.ComponentModel.DataAnnotations;

namespace IdentityServer.Models.DTOs.Request;

public class ClientCreationRequestDto
{
    [Required]
    public string ClientId { get; set; } = null!;

    [Required]
    public string ClientName { get; set; } = null!;

    public ICollection<string> AllowedGrantTypes { get; set; } = null!;

    public ICollection<string> AllowedScopes { get; set; } = null!;

    public ICollection<string> RedirectUris { get; set; } = null!;

    public ICollection<string> PostLogoutRedirectUris { get; set; } = null!;

    public ICollection<string> AllowedCorsOrigins { get; set; } = null!;

    public bool RequirePkce { get; set; }

    public bool RequireClientSecret { get; set; }
}