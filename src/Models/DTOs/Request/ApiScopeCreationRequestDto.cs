using System.ComponentModel.DataAnnotations;

namespace IdentityServer.Models.DTOs.Request;

public class ApiScopeCreationRequestDto
{
    [Required]
    public string Name { get; set; } = null!;

    public string DisplayName { get; set; } = null!;

    public string Description { get; set; } = null!;

    public IEnumerable<string> ApiResources { get; set; } = null!;
}
