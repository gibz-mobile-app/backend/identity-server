using System.ComponentModel.DataAnnotations;

namespace IdentityServer.Models.DTOs.Request;
public class ApiResourceCreationRequestDto
{
    [Required]
    public string Name { get; set; } = null!;

    [Required]
    public string DisplayName { get; set; } = null!;

    public string[] Scopes { get; set; } = null!;
}