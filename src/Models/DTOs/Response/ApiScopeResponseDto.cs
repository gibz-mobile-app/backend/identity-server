namespace IdentityServer.Models.DTOs.Response;

public class ApiScopeResponseDto
{
    public string Name { get; set; } = null!;
    public string DisplayName { get; set; } = null!;
    public string Description { get; set; } = null!;
}
