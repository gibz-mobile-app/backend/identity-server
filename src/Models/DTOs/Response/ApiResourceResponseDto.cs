namespace IdentityServer.Models.DTOs.Response;

public class ApiResourceResponseDto
{
    public string Name { get; set; } = null!;
    public string DisplayName { get; set; } = null!;
    public string[] Scopes { get; set; } = null!;
}