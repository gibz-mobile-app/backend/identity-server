namespace IdentityServer.Models.DTOs.Response;

public class ClientWithSecretResponseDto : ClientResponseDto
{
    public ICollection<string> ClientSecrets { get; set; } = null!;
}