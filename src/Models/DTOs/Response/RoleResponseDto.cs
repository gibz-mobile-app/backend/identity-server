namespace IdentityServer.Models.DTOs.Response;

public class RoleResponseDto
{
    public Guid Id { get; set; }
    public string Name { get; set; } = null!;
}
