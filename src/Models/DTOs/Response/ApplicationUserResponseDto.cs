namespace IdentityServer.Models.DTOs.Response;

public class ApplicationUserResponseDto
{
    public string Id { get; set; } = null!;
    public string UserName { get; set; } = null!;
    public string DisplayName { get; set; } = null!;
    public string Email { get; set; } = null!;
    public ICollection<RoleResponseDto> UserRoles { get; set; } = null!;
}