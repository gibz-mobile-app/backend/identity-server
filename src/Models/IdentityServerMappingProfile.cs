using AutoMapper;
using Duende.IdentityServer.Models;
using IdentityServer.Models.DTOs.Request;
using IdentityServer.Models.DTOs.Response;

namespace IdentityServer.Models;

public class IdentityServerMappingProfile : Profile
{
    public IdentityServerMappingProfile()
    {
        CreateMap<ApiResourceCreationRequestDto, ApiResource>();
        CreateMap<ApiResource, ApiResourceResponseDto>();


        CreateMap<Duende.IdentityServer.Models.Secret, string>()
            .ConvertUsing(secret => secret.Value);

        CreateMap<ClientCreationRequestDto, Client>();
        CreateMap<Client, ClientResponseDto>();
        CreateMap<Client, ClientWithSecretResponseDto>();

        CreateMap<ApiScopeCreationRequestDto, ApiScope>();
        CreateMap<ApiScope, ApiScopeResponseDto>();

        CreateMap<RoleCreationRequestDto, ApplicationRole>();
        CreateMap<ApplicationRole, RoleResponseDto>();

        CreateMap<ApplicationUser, ApplicationUserResponseDto>()
            .ForMember(dest => dest.UserRoles, opt => opt.MapFrom(src => src.UserRoles.Select(userRole => userRole.Role)));
    }
}