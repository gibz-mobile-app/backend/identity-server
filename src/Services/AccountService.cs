using System.Security.Claims;
using IdentityModel;
using IdentityServer.Models;
using Microsoft.AspNetCore.Identity;

namespace IdentityServer.Services;

public class AccountService(UserManager<ApplicationUser> _userManager) : IAccountService
{

    private readonly string[] _externalClaimsToPersistLocally = [JwtClaimTypes.Name, JwtClaimTypes.FamilyName, JwtClaimTypes.GivenName, "upn"];

    /// <inheritdoc />
    public async Task<ApplicationUser?> CreateUserForExternalAuthenticatedUserAsync(ClaimsPrincipal principal, string loginProvider)
    {
        // Instantiate the user.
        var user = new ApplicationUser
        {
            UserName = principal.FindFirstValue(ClaimTypes.Email),
            Email = principal.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value,
            DisplayName = principal.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name)?.Value
        };


        // Flag users email as confirmed since it originates from an external identity provider.
        if (user.Email is not null)
        {
            user.EmailConfirmed = true;
        }

        // Persist the user in the database.
        var createUserResult = await _userManager.CreateAsync(user);
        if (createUserResult.Succeeded)
        {
            // Add claims
            var claims = principal.Claims.Where(claim => _externalClaimsToPersistLocally.Contains(claim.Type)).ToArray();
            await _userManager.AddClaimsAsync(user, claims);

            // Add the external provider as login for the new user.
            createUserResult = await _userManager.AddLoginAsync(
                user,
                new ExternalLoginInfo(
                    principal,
                    loginProvider,
                    principal.FindFirstValue(ClaimTypes.NameIdentifier),
                    loginProvider
                )
            );

            if (!createUserResult.Succeeded)
            {
                return null;
            }
        }

        return user;
    }

    /// <inheritdoc />
    public async Task<ApplicationUser?> GetUserForExternalLoginAsync(string provider, string providerUserId)
    {
        var user = await _userManager.FindByLoginAsync(provider, providerUserId);
        return user;
    }

    public async Task<bool> UpdateClaims(ApplicationUser user, ClaimsPrincipal externalUser)
    {
        if (_externalClaimsToPersistLocally.Length > 0)
        {
            var userClaims = await _userManager.GetClaimsAsync(user);
            bool refreshSignIn = false;

            foreach (var claimType in _externalClaimsToPersistLocally)
            {
                var userClaim = userClaims.FirstOrDefault(c => c.Type == claimType);

                if (externalUser.HasClaim(c => c.Type == claimType))
                {
                    var externalClaim = externalUser.FindFirst(claimType);

                    if (userClaim == null)
                    {
                        await _userManager.AddClaimAsync(user, new Claim(claimType, externalClaim!.Value));
                        refreshSignIn = true;
                    }
                    else if (userClaim.Value != externalClaim!.Value)
                    {
                        await _userManager.ReplaceClaimAsync(user, userClaim, externalClaim);
                        refreshSignIn = true;
                    }
                }
            }

            return refreshSignIn;
        }

        return false;
    }
}