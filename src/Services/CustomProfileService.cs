using System.Security.Claims;
using Duende.IdentityServer.AspNetIdentity;
using Duende.IdentityServer.Models;
using IdentityModel;
using IdentityServer.Models;
using Microsoft.AspNetCore.Identity;

namespace IdentityServer.Services;

public class CustomProfileService(UserManager<ApplicationUser> _userManager, IUserClaimsPrincipalFactory<ApplicationUser> _claimsFactory, RoleManager<ApplicationRole> _roleManager)
    : ProfileService<ApplicationUser>(_userManager, _claimsFactory)
{

    /// <inheritdoc />
    protected override async Task GetProfileDataAsync(ProfileDataRequestContext context, ApplicationUser user)
    {
        await base.GetProfileDataAsync(context, user);

        string[] claimTypes = [JwtClaimTypes.Name, JwtClaimTypes.FamilyName, JwtClaimTypes.GivenName, "upn"];
        IList<Claim> claims = [];
        foreach (var claimType in claimTypes)
        {
            var value = context.Subject.FindFirstValue(claimType);
            if (!string.IsNullOrEmpty(value))
            {
                var claim = new Claim(claimType, value);
                if (claimType == "upn") {
                    // Do always emit the claim "upn"
                    context.IssuedClaims.Add(claim);
                }
                else
                {
                    claims.Add(claim);
                }
            }
        }

        context.AddRequestedClaims(claims);

        await AddRoleClaimsAsync(context, user);
    }

    /// <summary>
    /// Add the users roles and associated claims.
    /// </summary>
    /// <param name="context"></param>
    /// <param name="user"></param>
    private async Task AddRoleClaimsAsync(ProfileDataRequestContext context, ApplicationUser user)
    {
        var roles = await UserManager.GetRolesAsync(user);
        var claims = new List<Claim>();
        foreach (var roleName in roles)
        {
            // Add the users role as claim.
            claims.Add(new Claim(JwtClaimTypes.Role, roleName));

            var role = await _roleManager.FindByNameAsync(roleName);
            if (role is not null)
            {
                // Retrieve claims which are associated with the role.
                var rolesClaims = await _roleManager.GetClaimsAsync(role);
                claims.AddRange(rolesClaims);
            }
        }

        context.IssuedClaims.AddRange(claims);
    }
}
