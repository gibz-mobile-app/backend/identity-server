using System.Security.Claims;
using IdentityServer.Models;
using Microsoft.AspNetCore.Identity;

namespace IdentityServer.Services;

public interface IAccountService
{
    /// <summary>
    /// Create and persist a local user with an external login associated.
    /// </summary>
    /// <param name="principal">The principal to create the local user for.</param>
    /// <param name="provider">The authentication provider to be associated as login.</param>
    /// <returns>The newly created user.</returns>
    Task<ApplicationUser?> CreateUserForExternalAuthenticatedUserAsync(ClaimsPrincipal principal, string provider);

    /// <summary>
    /// Retrieve the local user for a given authentication provider and related user id.
    /// </summary>
    /// <param name="provider">The key of the authentication provider.</param>
    /// <param name="providerUserId">The unique user id for the specified provider.</param>
    /// <returns>The local user related to the providers user id.</returns>
    Task<ApplicationUser?> GetUserForExternalLoginAsync(string provider, string providerUserId);

    /// <summary>
    /// Updates the persisted claims for the given user based on the data available in the external User.
    /// The return value might be used as an indicator for refreshing the signIn.
    /// </summary>
    /// <param name="user">The locally authenticated user.</param>
    /// <param name="externalUser">The externally authenticated user identity.</param>
    /// <returns>Whether any claims were added or updated.</returns>
    Task<bool> UpdateClaims(ApplicationUser user, ClaimsPrincipal externalUser);
}