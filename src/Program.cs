using System.Security.Claims;
using IdentityModel;
using IdentityServer.Data;
using IdentityServer.Models;
using IdentityServer.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;

var builder = WebApplication.CreateBuilder(args);

builder.Services.Configure<ForwardedHeadersOptions>(options =>
{
    options.KnownNetworks.Clear();
    options.KnownProxies.Clear();
    options.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto | ForwardedHeaders.XForwardedHost;
});

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();

// Add AutoMapper for dependency injection
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

builder.Services.AddScoped<IAccountService, AccountService>();

ServerVersion sqlServerVersion = new MySqlServerVersion(new Version(8, 0, 26));
string identityServerConnectionString = GetConnectionString("_IDENTITY");

builder.Services.AddDbContext<IdentityDbContext>((serviceProvider, dbContextOptionsBuilder) =>
{
    dbContextOptionsBuilder.UseMySql(identityServerConnectionString, sqlServerVersion);
});

builder.Services.AddIdentity<ApplicationUser, ApplicationRole>()
    .AddDefaultTokenProviders()
    .AddSignInManager()
    .AddEntityFrameworkStores<IdentityDbContext>();

builder.Services.AddAuthentication(options =>
    {
        options.DefaultScheme = IdentityConstants.ApplicationScheme;
        options.DefaultSignInScheme = IdentityConstants.ExternalScheme;
    })
    .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options =>
    {
        options.MetadataAddress = builder.Configuration["Authentication:JwtBearer:MetadataAddress"];
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateAudience = false,
            ValidateIssuerSigningKey = false,
            RequireSignedTokens = false,
            ValidateIssuer = false,
        };
    })
    .AddMicrosoftAccount(microsoftOptions =>
    {
        microsoftOptions.ClientId = builder.Configuration["Authentication:Microsoft:ClientId"] ?? "";
        microsoftOptions.ClientSecret = builder.Configuration["Authentication:Microsoft:ClientSecret"] ?? "";
        microsoftOptions.AuthorizationEndpoint = builder.Configuration["Authentication:Microsoft:AuthorizationEndpoint"] ?? "";
        microsoftOptions.TokenEndpoint = builder.Configuration["Authentication:Microsoft:TokenEndpoint"] ?? "";
        microsoftOptions.SignInScheme = IdentityConstants.ExternalScheme;

        microsoftOptions.Scope.Add("openid");
        microsoftOptions.Scope.Add("profile");
        microsoftOptions.Scope.Add("User.Read");

        microsoftOptions.ClaimActions.MapJsonKey(JwtClaimTypes.Name, "displayName");
        microsoftOptions.ClaimActions.MapJsonKey(JwtClaimTypes.GivenName, "givenName");
        microsoftOptions.ClaimActions.MapJsonKey(JwtClaimTypes.FamilyName, "surname");
        microsoftOptions.ClaimActions.MapJsonKey("upn", "userPrincipalName");
        microsoftOptions.ClaimActions.MapJsonKey(JwtClaimTypes.Role, JwtClaimTypes.Role);
    })
    .AddScheme<UserIdAuthenticationOptions, UserIdAuthenticationHandler>(UserIdAuthenticationOptions.DefaultScheme, options =>
    {
        options.AllowedUserIds = builder.Configuration[UserIdAuthenticationOptions.UserIdConfigurationOption] ?? "";
    });

builder.Services.AddAuthorization(options =>
{
    // Auth policy which authorizes users with the idsrvAdmin role assigned OR with an user ID listed in the corresponding configuration variable.
    options.AddPolicy("idsrvAdmin", policy =>
    {
        policy.RequireAssertion(context =>
        {
            var idsrvAdminUserIds = (builder.Configuration["Authorization:IdsrvAdminUserIds"] ?? "").Split(',');
            if (context.Resource is HttpContext httpContext)
            {
                if (httpContext.Request.Headers.TryGetValue("X-IdsrvAdminUserId", out StringValues idsrvAdminUserId))
                {
                    if (idsrvAdminUserIds.Contains(idsrvAdminUserId.ToString()))
                    {
                        return true;
                    }
                }
            }
            return context.User.IsInRole("idsrvAdmin")
            || context.User.HasClaim(claim =>
                {
                    return claim.Type == ClaimTypes.NameIdentifier && idsrvAdminUserIds.Contains(claim.Value);
                });
        });
        policy.AuthenticationSchemes.Clear();
        policy.AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme, UserIdAuthenticationOptions.DefaultScheme);
    });
});

builder.Services.AddIdentityServer(options =>
    {
        options.EmitStaticAudienceClaim = true;
        options.UserInteraction.LoginUrl = "/login";
        options.UserInteraction.LogoutUrl = "/logout";
    })
    .AddAspNetIdentity<ApplicationUser>()
    .AddProfileService<CustomProfileService>()
    .AddConfigurationStore<ConfigurationStoreDbContext>(options =>
    {
        options.ConfigureDbContext = optionsBuilder =>
        {
            var connectionString = GetConnectionString("_IDSRV_CONFIGURATION");
            optionsBuilder.UseMySql(connectionString, sqlServerVersion);
        };
    })
    .AddOperationalStore<OperationalStoreDbContext>(options =>
    {
        options.ConfigureDbContext = optionsBuilder =>
        {
            var connectionString = GetConnectionString("_IDSRV_OPERATIONAL");
            optionsBuilder.UseMySql(connectionString, sqlServerVersion);
        };
    })
    .AddDeveloperSigningCredential();

var app = builder.Build();

app.UseForwardedHeaders();

app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.UseIdentityServer();

app.MapControllers();

app.MapFallbackToFile("index.html");

app.Run();

// Builds and returns the connection string based on environmentvariables.
// Since there are multiple connectionstrings to be built, the dbConnectionSuffix parameter may be used to distinguish the environment variables.
string GetConnectionString(string dbConnectionSuffix = "")
{
    Dictionary<string, string> attributeNames = new() {
        {"DB_HOST", "Server"},
        {"DB_PORT", "Port"},
        {"DB_NAME", "Database"},
        {"DB_USER", "User"},
        {"DB_PASS", "Password"}
    };

    Dictionary<string, string> attributes = new Dictionary<string, string>();

    foreach (var attributeName in attributeNames)
    {
        string? value = builder.Configuration[attributeName.Key + dbConnectionSuffix]
                        ?? builder.Configuration[attributeName.Key];
        if (!string.IsNullOrEmpty(value))
        {
            attributes.Add(attributeName.Value, value);
        }
    }

    List<string> connectionStringFragments = attributes.Select(entry => string.Format("{0}='{1}'", entry.Key, entry.Value)).ToList();
    string connectionString = string.Join(';', connectionStringFragments) + ";";

    return connectionString;
}