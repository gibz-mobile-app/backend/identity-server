using Duende.IdentityServer.EntityFramework.Mappers;
using Duende.IdentityServer.Models;
using IdentityServer.Data;
using Microsoft.AspNetCore.Mvc;

namespace IdentityServer.Controllers;

[ApiController]
[Route("[controller]")]
public class SeedingController : Controller
{
    private readonly ConfigurationStoreDbContext _configurationStoreDbContext;

    public SeedingController(ConfigurationStoreDbContext configurationStoreDbContext)
    {
        _configurationStoreDbContext = configurationStoreDbContext;
    }

    [HttpGet("initialData")]
    public async Task<IActionResult> SeedInitialData()
    {
        if (!_configurationStoreDbContext.IdentityResources.Any())
        {
            _configurationStoreDbContext.IdentityResources.Add((new IdentityResources.OpenId()).ToEntity());
            _configurationStoreDbContext.IdentityResources.Add((new IdentityResources.Profile()).ToEntity());
            _configurationStoreDbContext.IdentityResources.Add((new IdentityResources.Email()).ToEntity());

            await _configurationStoreDbContext.SaveChangesAsync();
        }

        if (!_configurationStoreDbContext.ApiScopes.Any())
        {
            _configurationStoreDbContext.ApiScopes.Add((new ApiScope("idsrvAdmin", "Identity Server Administrator")).ToEntity());
            _configurationStoreDbContext.ApiResources.Add((new ApiResource("idsrv", "Identity Server")).ToEntity());

            await _configurationStoreDbContext.SaveChangesAsync();
        }

        return Ok();
    }
}