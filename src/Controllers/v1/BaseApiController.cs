using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace IdentityServer.Controllers.v1;

[ApiController]
[Route("v1/[controller]")]
public abstract class BaseApiController : Controller
{
    protected readonly IMapper _mapper;

    protected BaseApiController(IMapper mapper)
    {
        _mapper = mapper;
    }
}
