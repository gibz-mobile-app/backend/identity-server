using AutoMapper;
using Duende.IdentityServer.EntityFramework.Entities;
using Duende.IdentityServer.EntityFramework.Mappers;
using IdentityServer.Data;
using IdentityServer.Models.DTOs.Request;
using IdentityServer.Models.DTOs.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ApiScopeModel = Duende.IdentityServer.Models.ApiScope;

namespace IdentityServer.Controllers.v1;

[Authorize(Policy = "idsrvAdmin")]
public class ApiScopeController : BaseApiController
{
    private readonly ConfigurationStoreDbContext _configurationStore;

    public ApiScopeController(IMapper mapper, ConfigurationStoreDbContext configurationStoreDbContext) : base(mapper)
    {
        _configurationStore = configurationStoreDbContext;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<ApiScopeResponseDto>>> ListApiScopesAsync()
    {
        var apiScopeEntities = await _configurationStore.ApiScopes.ToListAsync();
        var apiScopes = apiScopeEntities.Select(entity => entity.ToModel());

        return _mapper.Map<List<ApiScopeResponseDto>>(apiScopes);
    }

    [HttpGet("{scopeName}")]
    public async Task<ActionResult<ApiScopeResponseDto>> GetApiScopeAsync(string scopeName)
    {
        var apiScopeEntity = await _configurationStore.ApiScopes.FirstOrDefaultAsync(scope => scope.Name == scopeName);

        if (apiScopeEntity is null)
        {
            return NotFound();
        }

        var apiScope = apiScopeEntity.ToModel();

        return _mapper.Map<ApiScopeResponseDto>(apiScope);
    }

    [HttpPost]
    public async Task<ActionResult<ApiScopeResponseDto>> CreateApiScope(ApiScopeCreationRequestDto apiScopeCreationRequestDto)
    {
        ApiScopeModel apiScope = _mapper.Map<ApiScopeModel>(apiScopeCreationRequestDto);

        // Add ApiScope
        await _configurationStore.ApiScopes.AddAsync(apiScope.ToEntity());

        // Add ApiScope as ApiResourceScope to ApiResource
        if (apiScopeCreationRequestDto.ApiResources is not null)
        {
            foreach (string apiResourceName in apiScopeCreationRequestDto.ApiResources)
            {
                var apiResourceEntity = await _configurationStore.ApiResources.FirstOrDefaultAsync(apiResource => apiResource.Name == apiResourceName);
                if (apiResourceEntity is not null)
                {
                    var apiResourceScope = new ApiResourceScope() { Scope = apiScope.Name };
                    if (apiResourceEntity.Scopes is null)
                    {
                        apiResourceEntity.Scopes = new List<ApiResourceScope>();
                    }
                    apiResourceEntity.Scopes.Add(apiResourceScope);
                }
            }
        }

        // Persist entities
        await _configurationStore.SaveChangesAsync();

        return _mapper.Map<ApiScopeResponseDto>(apiScope);
    }

    [HttpDelete]
    public async Task<IActionResult> DeleteApiScope([FromQuery] string name)
    {
        if (string.IsNullOrEmpty(name))
        {
            return BadRequest();
        }

        var apiScope = await _configurationStore.ApiScopes.FirstOrDefaultAsync(apiScope => apiScope.Name.Equals(name));

        if (apiScope is null)
        {
            return NotFound();
        }

        _configurationStore.ApiScopes.Remove(apiScope);
        await _configurationStore.SaveChangesAsync();

        return NoContent();
    }
}
