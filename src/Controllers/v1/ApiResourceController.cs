using AutoMapper;
using Duende.IdentityServer.EntityFramework.Mappers;
using Duende.IdentityServer.Models;
using IdentityServer.Data;
using IdentityServer.Models.DTOs.Request;
using IdentityServer.Models.DTOs.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IdentityServer.Controllers.v1;

[Authorize(Policy = "idsrvAdmin")]
public class ApiResourceController : BaseApiController
{
    private readonly ConfigurationStoreDbContext _configurationStore;

    public ApiResourceController(IMapper mapper, ConfigurationStoreDbContext configurationStoreDbContext) : base(mapper)
    {
        _configurationStore = configurationStoreDbContext;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<ApiResourceResponseDto>>> ListApiResources()
    {
        var apiResourceEntities = await _configurationStore.ApiResources.Include(apiResource => apiResource.Scopes).ToListAsync();
        var apiResources = apiResourceEntities.Select(entity => entity.ToModel());

        return _mapper.Map<List<ApiResourceResponseDto>>(apiResources);
    }

    [HttpPost]
    public async Task<ActionResult<ApiResourceResponseDto>> CreateApiResource(ApiResourceCreationRequestDto apiResourceCreationRequestDto)
    {
        ApiResource apiResource = _mapper.Map<ApiResource>(apiResourceCreationRequestDto);

        // Add api scopes
        List<Duende.IdentityServer.EntityFramework.Entities.ApiScope> apiScopes =
            apiResource.Scopes.Select(scopeName => (new ApiScope(scopeName)).ToEntity()).ToList();
        await _configurationStore.ApiScopes.AddRangeAsync(apiScopes);

        // Add api resources
        await _configurationStore.ApiResources.AddAsync(apiResource.ToEntity());

        // Persist entities
        await _configurationStore.SaveChangesAsync();

        return _mapper.Map<ApiResourceResponseDto>(apiResource);
    }

    [HttpDelete]
    public async Task<IActionResult> DeleteApiResource([FromQuery] string name)
    {
        if (string.IsNullOrEmpty(name))
        {
            return BadRequest();
        }

        var apiResource = await _configurationStore.ApiResources.FirstOrDefaultAsync(apiResource => apiResource.Name.Equals(name));

        if (apiResource is null)
        {
            return NotFound();
        }

        _configurationStore.ApiResources.Remove(apiResource);
        await _configurationStore.SaveChangesAsync();

        return NoContent();
    }
}