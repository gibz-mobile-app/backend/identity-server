using AutoMapper;
using IdentityServer.Models;
using IdentityServer.Models.DTOs.Request;
using IdentityServer.Models.DTOs.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IdentityServer.Controllers.v1;

[Authorize(Policy = "idsrvAdmin")]
public class RoleController : BaseApiController
{
    private readonly RoleManager<ApplicationRole> _roleManager;

    public RoleController(IMapper mapper, RoleManager<ApplicationRole> roleManager) : base(mapper)
    {
        _roleManager = roleManager;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<RoleResponseDto>>> ListRoles()
    {
        var roles = await _roleManager.Roles.ToListAsync();

        return _mapper.Map<List<RoleResponseDto>>(roles);
    }

    [HttpGet("/roleName")]
    public async Task<ActionResult<RoleResponseDto>> GetRole(string roleName)
    {
        var role = await _roleManager.FindByNameAsync(roleName);

        if (role is null)
        {
            return NotFound();
        }

        return _mapper.Map<RoleResponseDto>(role);
    }

    [HttpPost]
    public async Task<ActionResult<RoleResponseDto>> CreateRole(RoleCreationRequestDto roleCreationRequestDto)
    {
        var role = _mapper.Map<ApplicationRole>(roleCreationRequestDto);

        IdentityResult creationResult = await _roleManager.CreateAsync(role);

        if (!creationResult.Succeeded)
        {
            var errors = creationResult.Errors.Select(error => $"{error.Code}: {error.Description}");
            var errorMessage = $"Creation failed: " + string.Join(';', errors);
            return BadRequest(errorMessage);
        }

        return CreatedAtAction(nameof(GetRole), _mapper.Map<RoleResponseDto>(role));
    }

    [HttpDelete]
    public async Task<IActionResult> DeleteRole([FromQuery] string roleName)
    {
        if (string.IsNullOrEmpty(roleName))
        {
            return BadRequest();
        }

        var role = await _roleManager.FindByNameAsync(roleName);

        if (role is null)
        {
            return NotFound();
        }

        var deletionResult = await _roleManager.DeleteAsync(role);

        if (!deletionResult.Succeeded)
        {
            var errors = deletionResult.Errors.Select(error => $"{error.Code}: {error.Description}");
            var errorMessage = $"Deletion failed: " + string.Join(';', errors);
            return BadRequest(errorMessage);
        }

        return NoContent();
    }
}