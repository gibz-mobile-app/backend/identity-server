using AutoMapper;
using Duende.IdentityServer.EntityFramework.Mappers;
using Duende.IdentityServer.Models;
using IdentityServer.Data;
using IdentityServer.Models.DTOs.Request;
using IdentityServer.Models.DTOs.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IdentityServer.Controllers.v1;

[Authorize(Policy = "idsrvAdmin")]
public class ClientController : BaseApiController
{
    private readonly ConfigurationStoreDbContext _configurationStore;

    public ClientController(IMapper mapper, ConfigurationStoreDbContext configurationStoreDbContext) : base(mapper)
    {
        _configurationStore = configurationStoreDbContext;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<ClientResponseDto>>> ListClients()
    {
        var clientEntities = await _configurationStore.Clients.Include(client => client.AllowedGrantTypes)
                                                              .Include(client => client.AllowedScopes)
                                                              .Include(client => client.RedirectUris)
                                                              .Include(client => client.PostLogoutRedirectUris)
                                                              .Include(client => client.AllowedCorsOrigins)
                                                              .ToListAsync();
        var clients = clientEntities.Select(entity => entity.ToModel());

        return _mapper.Map<List<ClientResponseDto>>(clients);
    }

    [HttpPost]
    public async Task<ActionResult<ClientWithSecretResponseDto>> CreateClient(ClientCreationRequestDto clientCreationRequestDto)
    {
        Client client = _mapper.Map<Client>(clientCreationRequestDto);

        // Generate client secret
        var secret = Guid.NewGuid().ToString();
        client.ClientSecrets = new List<Secret> { new Secret(secret.Sha256()) };

        await _configurationStore.Clients.AddAsync(client.ToEntity());
        await _configurationStore.SaveChangesAsync();

        var response = _mapper.Map<ClientWithSecretResponseDto>(client);
        response.ClientSecrets = new List<string> { secret };

        return response;
    }

    [HttpDelete]
    public async Task<IActionResult> DeleteApiResource([FromQuery] string clientId)
    {
        if (string.IsNullOrEmpty(clientId))
        {
            return BadRequest();
        }

        var client = await _configurationStore.Clients.FirstOrDefaultAsync(client => client.ClientId.Equals(clientId));

        if (client is null)
        {
            return NotFound();
        }

        _configurationStore.Clients.Remove(client);
        await _configurationStore.SaveChangesAsync();

        return NoContent();
    }
}
