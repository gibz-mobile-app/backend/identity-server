using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Duende.IdentityServer;
using Duende.IdentityServer.Events;
using Duende.IdentityServer.Services;
using IdentityModel;
using IdentityServer.Models;
using IdentityServer.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace IdentityServer.Controllers.v1;

[ApiController]
[Route("v1")]
public class AccountController(
    IIdentityServerInteractionService _interactionService,
    IAccountService _accountService,
    IEventService _eventService,
    SignInManager<ApplicationUser> _signInManager,
    ILogger<AccountController> _logger
    ) : Controller
{

    [HttpPost("logout")]
    public async Task<IActionResult> Logout([FromBody] string logoutId)
    {
        var logoutInfo = await _interactionService.GetLogoutContextAsync(logoutId);
        await HttpContext.SignOutAsync();

        return Ok(new
        {
            iframeUrl = logoutInfo?.SignOutIFrameUrl,
            postLogoutRedirectUri = logoutInfo?.PostLogoutRedirectUri
        });
    }

    /// <summary>
    /// Action to be called when the user decides to sign in with M365 as authentication provider.
    /// This usually happens as a click/tap in the web ui.
    /// </summary>
    /// <param name="returnUrl">URL to redirect the authentication flow after successful authentication.</param>
    [HttpGet("login/microsoft")]
    public ChallengeResult LoginWithMicrosoft([FromQuery] string returnUrl)
    {
        var callbackUrl = Url.Action(nameof(HandleMicrosoftCallback));

        var props = new AuthenticationProperties
        {
            RedirectUri = callbackUrl,
            Items =
            {
                { "LoginProvider", Microsoft.AspNetCore.Authentication.MicrosoftAccount.MicrosoftAccountDefaults.AuthenticationScheme},
                { "returnUrl", returnUrl },
            },
        };

        props.SetParameter("prompt", "select_account");

        return Challenge(props, Microsoft.AspNetCore.Authentication.MicrosoftAccount.MicrosoftAccountDefaults.AuthenticationScheme);
    }

    /// <summary>
    /// Handles the authentication response from Microsoft.
    /// </summary>
    /// <returns>Redirect to the previously specified return url.</returns>
    /// <exception cref="Exception">In case the authentication failed.</exception>
    [HttpGet("microsoft/handleCallback")]
    public async Task<IActionResult> HandleMicrosoftCallback()
    {
        // Use the cookie to retrieve the external authentication status.
        var result = await HttpContext.AuthenticateAsync(IdentityConstants.ExternalScheme);
        if (result?.Succeeded != true)
        {
            throw new Exception("External authentication error");
        }

        var externalUser = result.Principal ?? throw new Exception("External user not available");

        // The user id provided by microsoft is required to identify the user.
        var userIdClaim = externalUser.FindFirst(JwtClaimTypes.Subject)
                          ?? externalUser.FindFirst(ClaimTypes.NameIdentifier)
                          ?? throw new Exception("Unknown userid");

        var providerUserId = userIdClaim.Value;
        var provider = result.Properties?.Items["LoginProvider"]
                       ?? Microsoft.AspNetCore.Authentication.MicrosoftAccount.MicrosoftAccountDefaults.AuthenticationScheme;

        // Retrieve existing user from database.
        var user = await _accountService.GetUserForExternalLoginAsync(provider, providerUserId);

        if (user is null)
        {
            // There's no local user account for externally authenticated user yet --> Create it.
            user = await _accountService.CreateUserForExternalAuthenticatedUserAsync(externalUser, provider);
        }
        else
        {
            // The user is already known. Check for (persistent) claims which might need to be updated.
            if (await _accountService.UpdateClaims(user, externalUser))
            {
                await _signInManager.RefreshSignInAsync(user);
            }
        }

        // Generate additional claims based on the data Microsoft is providing.
        var additionalLocalClaims = new List<Claim>();
        additionalLocalClaims.AddRange(externalUser.Claims);

        var localSingInProperties = new AuthenticationProperties();
        CaptureExternalLoginContext(result, additionalLocalClaims, localSingInProperties);

        var identityServerUser = new IdentityServerUser(user.Id.ToString())
        {
            DisplayName = user.DisplayName,
            IdentityProvider = provider,
            AdditionalClaims = additionalLocalClaims,
        };

        // Start the local auth session.
        await HttpContext.SignInAsync(identityServerUser, localSingInProperties);

        // Delete temporary cookie used during external authentication
        await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

        // Retrieve return URL
        var returnUrl = result.Properties?.Items["returnUrl"] ?? "~/";

        // Check if external login is in the context of an OIDC request
        var context = await _interactionService.GetAuthorizationContextAsync(returnUrl);
        await _eventService.RaiseAsync(new UserLoginSuccessEvent(provider, providerUserId, user.Id.ToString(), user.UserName, true, context?.Client.ClientId));

        return Redirect(returnUrl);
    }


    // If the external login is OIDC-based, there are certain things we need to preserve to make logout work.
    private void CaptureExternalLoginContext(AuthenticateResult externalResult, List<Claim> localClaims, AuthenticationProperties localSignInProps)
    {
        // if the external system sent a session id claim, copy it over
        // so we can use it for single sign-out
        var sid = externalResult.Principal?.Claims.FirstOrDefault(x => x.Type == JwtClaimTypes.SessionId);
        if (sid is not null)
        {
            localClaims.Add(new Claim(JwtClaimTypes.SessionId, sid.Value));
        }

        // if the external provider issued an id_token, we'll keep it for signout
        var idToken = externalResult.Properties?.GetTokenValue("id_token");
        if (idToken is not null)
        {
            localSignInProps.StoreTokens(new[] { new AuthenticationToken { Name = "id_token", Value = idToken } });
        }
    }
}