using AutoMapper;
using IdentityServer.Data;
using IdentityServer.Models;
using IdentityServer.Models.DTOs.Request;
using IdentityServer.Models.DTOs.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IdentityServer.Controllers.v1;

[Authorize(Policy = "idsrvAdmin")]
public class UserController : BaseApiController
{
    private readonly IdentityDbContext _context;
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly RoleManager<ApplicationRole> _roleManager;

    public UserController(IMapper mapper, IdentityDbContext context, UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager) : base(mapper)
    {
        _context = context;
        _userManager = userManager;
        _roleManager = roleManager;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<ApplicationUserResponseDto>>> ListUsers()
    {
        // var users = await _userManager.Users.Include(user => user.UserRoles).ToListAsync();
        var users = await _context.Users.Include(user => user.UserRoles)
                                        .ThenInclude(userRole => userRole.Role)
                                        .ToListAsync();

        return _mapper.Map<List<ApplicationUserResponseDto>>(users);
    }

    [HttpGet("{userId}")]
    public async Task<ActionResult<ApplicationUserResponseDto>> GetUser(string userId)
    {
        var user = await _userManager.FindByIdAsync(userId);

        if (user is null)
        {
            return NotFound();
        }

        await _context.Entry<ApplicationUser>(user)
                      .Collection(user => user.UserRoles)
                      .Query()
                      .Include(userRole => userRole.Role)
                      .LoadAsync();

        return _mapper.Map<ApplicationUserResponseDto>(user);

        // return await GetUserResponseDtoWithRolesAsync(user);
    }

    [HttpPut("{userId}")]
    public async Task<ActionResult<ApplicationUserResponseDto>> AddToRoles(string userId, RoleModificationRequestDto roleModificationRequestDto)
    {
        if (!userId.Equals(roleModificationRequestDto.UserId))
        {
            BadRequest();
        }

        var user = await _userManager.FindByIdAsync(userId);

        if (user is null)
        {
            return NotFound();
        }

        if (roleModificationRequestDto.RolesToBeAdded.Any())
        {
            var addResult = await _userManager.AddToRolesAsync(user, roleModificationRequestDto.RolesToBeAdded);

            if (!addResult.Succeeded)
            {
                var errors = addResult.Errors.Select(error => $"{error.Code}: {error.Description}");
                var errorMessage = $"Adding roles failed: " + string.Join(';', errors);
                return BadRequest(errorMessage);
            }
        }

        if (roleModificationRequestDto.RolesToBeRemoved.Any())
        {
            var removeResult = await _userManager.RemoveFromRolesAsync(user, roleModificationRequestDto.RolesToBeRemoved);

            if (!removeResult.Succeeded)
            {
                var errors = removeResult.Errors.Select(error => $"{error.Code}: {error.Description}");
                var errorMessage = $"Removing roles failed: " + string.Join(';', errors);
                return BadRequest(errorMessage);
            }
        }

        return _mapper.Map<ApplicationUserResponseDto>(user);

        // return await GetUserResponseDtoWithRolesAsync(user);
    }

    private async Task<ApplicationUserResponseDto> GetUserResponseDtoWithRolesAsync(ApplicationUser user)
    {
        var userDto = _mapper.Map<ApplicationUserResponseDto>(user);
        var rolesNames = await _userManager.GetRolesAsync(user);
        var roles = rolesNames.Select(async roleName => await _roleManager.FindByNameAsync(roleName));
        var roleDtos = _mapper.Map<IList<RoleResponseDto>>(roles);
        userDto.UserRoles = roleDtos;
        return userDto;
    }
}
