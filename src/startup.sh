#!/bin/sh
if [ "${ASPNETCORE_ENVIRONMENT+set}" = set ] && [ "$ASPNETCORE_ENVIRONMENT" != Production ]; then
  update-ca-certificates
fi

echo "Starting Identity Server..."
dotnet IdentityServer.dll